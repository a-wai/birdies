## Birdies

Birdies is a simple app for entering your score during a golf game.

![](data/screenshots/birdies-1.png)
![](data/screenshots/birdies-2.png)
![](data/screenshots/birdies-3.png)

__*DISCLAIMER: Birdies is currently in early development stages, and as such
should be considered a mere prototype, rather than a fully-functional
application.*__

### Features

For now, Birdies only allows one to enter their score during a game on the
selected course.

Scores are not saved to disk (nor are game statistics) and are therefore lost
when starting a new game and/or closing the application.

Courses are stored in JSON files under [data/courses](data/courses). Currently
only a handful of courses are available, contributions to expand the database
are obviously welcome!

Note: User-defined course definitions can be stored under
`$HOME/.loca/share/birdies/courses/` for local development and testing purposes.

### Dependencies

This application is written in Rust and uses GTK4 and libadwaita. It also
relies on Meson for its build system.

Building the application therefore requires the following packages:
* cargo
* GTK4 >= 4.12
* libadwaita >= 1.4
* meson
* ninja

On Debian systems, you can install those dependencies using the following
command:
```
sudo apt install libgtk-4-dev libadwaita-1-dev meson ninja-build cargo
```

### TODO

* Application icon (help appreciated, the current emoji-as-an-app-icon
  workaround isn't a long-term solution :wink: )
* AppStream-compliant metadata (`metainfo.xml`)
* Documentation
* Add localization support
* Manage and display game stats

### License

Birdies is released under the terms of the
[GNU General Public License v3](LICENSE).
