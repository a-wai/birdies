use std::fmt::{Display, Formatter, Result};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy, Eq, Hash, PartialEq)]
#[serde(rename_all = "lowercase")]
pub enum TeeColor {
    Black,
    #[default]
    White,
    Yellow,
    Blue,
    Red,
    Orange,
    Purple,
    Green,
}

impl Display for TeeColor {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let color = match self {
            TeeColor::Black => "black",
            TeeColor::White => "white",
            TeeColor::Yellow => "yellow",
            TeeColor::Blue => "blue",
            TeeColor::Red => "red",
            TeeColor::Orange => "orange",
            TeeColor::Purple => "purple",
            TeeColor::Green => "green",
        };

        write!(f, "{}", color)
    }
}

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy)]
pub enum DistanceUnit {
    #[default]
    #[serde(alias = "m")]
    Meters,
    #[serde(alias = "yds")]
    Yards,
}

impl Display for DistanceUnit {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            DistanceUnit::Meters => write!(f, "m"),
            DistanceUnit::Yards => write!(f, "yds"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy)]
pub struct Score {
    pub strokes: i32,
    pub putts: i32,
    pub gir: bool,
}

impl Score {
    pub fn add_stroke(&mut self) {
        self.strokes += 1;
    }

    pub fn add_putt(&mut self) {
        self.putts += 1;
    }

    pub fn set_gir(&mut self, gir: bool) {
        self.gir = gir;
    }
}

pub trait HasId {
    fn set_id(&mut self, id: &str);
}
