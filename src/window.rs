use glib::subclass::prelude::*;
use gtk::{
    gio,
    glib::{self, clone, closure_local},
    prelude::*,
};

use crate::app::BdApplication;
use crate::cards_view::BdCardsView;
use crate::course_view::BdCourseView;
use crate::score_view::BdScoreView;

mod imp {
    use adw::subclass::prelude::*;
    use gtk::{glib, CompositeTemplate};

    use crate::cards_view::BdCardsView;
    use crate::course_view::BdCourseView;
    use crate::score_view::BdScoreView;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/a-wai/Birdies/ui/window.ui")]
    pub struct BdWindow {
        #[template_child]
        pub button_done: TemplateChild<gtk::Button>,
        #[template_child]
        pub button_back: TemplateChild<gtk::Button>,
        #[template_child]
        pub main_stack: TemplateChild<adw::ViewStack>,
        #[template_child]
        pub game_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub score_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub courses_list: TemplateChild<BdCourseView>,
        #[template_child]
        pub cards_list: TemplateChild<BdCardsView>,
        #[template_child]
        pub game_score: TemplateChild<BdScoreView>,
        #[template_child]
        pub card_view: TemplateChild<BdScoreView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BdWindow {
        const NAME: &'static str = "BdWindow";
        type Type = super::BdWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BdWindow {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }

    impl WidgetImpl for BdWindow {}
    impl WindowImpl for BdWindow {}
    impl ApplicationWindowImpl for BdWindow {}
    impl AdwApplicationWindowImpl for BdWindow {}
}

glib::wrapper! {
    pub struct BdWindow(ObjectSubclass<imp::BdWindow>)
        @extends gtk::Widget, gtk::Window, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

#[gtk::template_callbacks]
impl BdWindow {
    pub fn new(app: &gtk::Application) -> Self {
        let win: BdWindow = glib::Object::builder().property("application", app).build();
        let imp = win.imp();

        let app = gio::Application::default()
            .expect("Failed to retrieve application singleton")
            .downcast::<BdApplication>()
            .unwrap();

        let db = app.get_db();

        for golf in db.golfs() {
            for course in golf.courses() {
                imp.courses_list.add_course(course);
            }
        }

        for card in db.cards() {
            imp.cards_list.add_card(card);
        }

        imp.courses_list.connect_closure(
            "course-selected",
            false,
            closure_local!(
                #[watch]
                win,
                move |cv: BdCourseView| {
                    let course = cv.selected_course();
                    win.imp().game_score.set_course(&course);
                    win.imp().game_stack.set_visible_child_name("score-view");
                }
            ),
        );

        imp.cards_list.connect_closure(
            "card-selected",
            false,
            closure_local!(
                #[watch]
                win,
                move |cv: BdCardsView| {
                    let card = cv.selected_card();
                    win.imp().card_view.set_card(&card);
                    win.imp().score_stack.set_visible_child_name("score-view");
                    win.imp().button_back.set_visible(true);
                }
            ),
        );

        imp.game_score.connect_closure(
            "game-completed",
            false,
            closure_local!(
                #[watch]
                win,
                move |sv: BdScoreView| {
                    let imp = win.imp();

                    imp.cards_list.add_card(&sv.card());
                    imp.button_done.set_visible(true);
                }
            ),
        );

        imp.main_stack.connect_visible_child_name_notify(clone!(
            #[weak]
            win,
            move |_| {
                let imp = win.imp();

                let child = imp.main_stack.visible_child_name().unwrap();
                if child == "play" {
                    imp.button_back.set_visible(false);
                } else if child == "scores" {
                    if imp.button_done.get_visible() {
                        win.on_done_button_clicked(&imp.button_done);
                    }
                    if imp.score_stack.visible_child_name().unwrap() == "score-view" {
                        imp.button_back.set_visible(true);
                    }
                }
            }
        ));

        win
    }

    #[template_callback]
    fn on_done_button_clicked(&self, button: &gtk::Button) {
        let imp = self.imp();
        imp.game_stack.set_visible_child_name("courses-view");
        button.set_visible(false);
    }

    #[template_callback]
    fn on_back_button_clicked(&self, button: &gtk::Button) {
        let imp = self.imp();
        imp.score_stack.set_visible_child_name("cards-view");
        button.set_visible(false);
    }
}
