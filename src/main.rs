mod app;
mod cards_view;
mod config;
mod course_view;
mod hole_entry;
mod score_view;
mod utils;
mod window;

mod model;

use adw::prelude::*;
use gtk::{gio, glib};

fn main() -> glib::ExitCode {
    let res = gio::Resource::load(config::RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&res);

    app::BdApplication::new().run()
}
