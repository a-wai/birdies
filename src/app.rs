use glib::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config;
use crate::model::database::BdDatabase;

mod imp {
    use adw::subclass::prelude::*;
    use gtk::{
        gio, glib,
        prelude::{ActionMapExt, ApplicationExt, Cast, GtkApplicationExt, GtkWindowExt},
    };
    use once_cell::sync::OnceCell;

    use crate::model::database::BdDatabase;
    use crate::window::BdWindow;

    #[derive(Default)]
    pub struct BdApplication {
        pub database: OnceCell<BdDatabase>,
        pub window: OnceCell<BdWindow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BdApplication {
        const NAME: &'static str = "BdApplication";
        type Type = super::BdApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for BdApplication {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }

    impl ApplicationImpl for BdApplication {
        fn activate(&self) {
            if self.window.get().is_none() {
                let window = BdWindow::new(self.obj().upcast_ref());
                window.present();
                self.window.set(window).unwrap();
            } else {
                self.window.get().unwrap().present();
            }
        }

        fn startup(&self) {
            let obj = self.obj();
            self.parent_startup();

            let action = gio::SimpleAction::new("quit", None);
            action.connect_activate({
                let app = obj.downgrade();
                move |_, _| {
                    let app = app.upgrade().unwrap();
                    app.quit();
                }
            });
            obj.add_action(&action);
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl GtkApplicationImpl for BdApplication {}
    impl AdwApplicationImpl for BdApplication {}
}

impl Default for BdApplication {
    fn default() -> Self {
        glib::Object::builder()
            .property("application-id", config::APP_ID)
            .property("flags", gio::ApplicationFlags::NON_UNIQUE)
            .property("resource-base-path", "/com/a-wai/Birdies")
            .build()
    }
}

glib::wrapper! {
    pub struct BdApplication(ObjectSubclass<imp::BdApplication>)
        @extends adw::Application, gtk::Application, gio::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl BdApplication {
    pub fn new() -> Self {
        let this = Self::default();
        this.imp().database.set(BdDatabase::new()).unwrap();
        this
    }

    pub fn get_db(&self) -> &BdDatabase {
        self.imp().database.get().unwrap()
    }
}
