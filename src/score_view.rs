use glib::subclass::prelude::*;
use gtk::prelude::ObjectExt;
use gtk::{gio, glib, prelude::Cast, prelude::WidgetExt};

use std::cell::Ref;

use crate::app::BdApplication;
use crate::hole_entry::BdHoleEntry;
use crate::model::score_card::BdScoreCard;

mod imp {
    use adw::subclass::prelude::*;
    use glib::subclass::Signal;
    use gtk::{glib, prelude::WidgetExt, CompositeTemplate};
    use once_cell::sync::Lazy;
    use std::cell::{Cell, RefCell};

    use crate::model::{course::BdCourse, score_card::BdScoreCard};
    use crate::utils::Score;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/a-wai/Birdies/ui/scoreview.ui")]
    pub struct BdScoreView {
        // Page header
        #[template_child]
        pub course_name: TemplateChild<gtk::Label>,
        #[template_child]
        pub tee_color: TemplateChild<gtk::Button>,
        #[template_child]
        pub course_details: TemplateChild<gtk::Label>,
        #[template_child]
        pub total_strokes: TemplateChild<gtk::Label>,
        #[template_child]
        pub through_hole: TemplateChild<gtk::Label>,
        #[template_child]
        pub total_score: TemplateChild<gtk::Label>,

        // Score entry area
        #[template_child]
        pub score_box: TemplateChild<gtk::Box>,
        #[template_child]
        pub hole_box: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub stroke_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub putt_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub done_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub skip_btn: TemplateChild<gtk::Button>,

        // Holes lists
        #[template_child]
        pub completed_holes: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub remaining_holes: TemplateChild<gtk::ListBox>,

        // Internal state
        pub score_card: RefCell<BdScoreCard>,
        pub course: RefCell<BdCourse>,
        pub hole_nr: Cell<usize>,
        pub par_so_far: Cell<i32>,
        pub score: Cell<Score>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BdScoreView {
        const NAME: &'static str = "BdScoreView";
        type Type = super::BdScoreView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BdScoreView {
        fn constructed(&self) {
            self.parent_constructed();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("game-completed").build()]);

            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for BdScoreView {}
    impl BoxImpl for BdScoreView {}

    impl BdScoreView {
        pub fn update_score(&self) {
            self.through_hole
                .set_label(&format!("T{}", self.hole_nr.get()));

            let score = self.score.get();
            let total_strokes = score.strokes + score.putts;
            let total_score = total_strokes - self.par_so_far.get();

            self.total_strokes.set_label(&format!("{}", total_strokes));
            self.total_score.set_label(&format!("({:+})", total_score));

            if total_score < -1 {
                self.total_score.set_css_classes(&["success"]);
            } else if total_score == -1 {
                self.total_score.set_css_classes(&["accent"]);
            } else if total_score == 1 {
                self.total_score.set_css_classes(&["warning"]);
            } else if total_score > 1 {
                self.total_score.set_css_classes(&["error"]);
            } else {
                self.total_score.set_css_classes(&[]);
            }
        }
    }
}

glib::wrapper! {
    pub struct BdScoreView(ObjectSubclass<imp::BdScoreView>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for BdScoreView {
    fn default() -> Self {
        Self::new()
    }
}

#[gtk::template_callbacks]
impl BdScoreView {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }

    fn set_data(&self, cid: Option<&String>, card: Option<&BdScoreCard>) {
        if cid.is_none() && card.is_none() {
            eprintln!("BdScoreView.set_data() called with None arguments!");
            return;
        }

        let imp = self.imp();

        imp.completed_holes.remove_all();
        imp.hole_box.remove_all();
        imp.remaining_holes.remove_all();
        imp.completed_holes.set_visible(false);
        imp.hole_box.set_visible(false);
        imp.score_box.set_visible(false);
        imp.remaining_holes.set_visible(false);

        let app = gio::Application::default()
            .expect("Failed to retrieve application singleton")
            .downcast::<BdApplication>()
            .unwrap();
        let db = app.get_db();

        let course = if card.is_some() {
            let newcard = card.unwrap();
            imp.score_card.replace(BdScoreCard::from(newcard));
            let c = db.course_by_id(newcard.id()).unwrap();
            c
        } else {
            let newcard = BdScoreCard::new(cid.unwrap());
            imp.score_card.replace(newcard);
            db.course_by_id(cid.unwrap()).unwrap()
        };
        imp.hole_nr.set(0);
        imp.par_so_far.set(0);

        imp.course.replace(course.clone());
        imp.score.set(imp.score_card.borrow().get_total_score());

        imp.course_name.set_label(course.name());
        imp.tee_color.set_css_classes(&["circular", "opaque"]);
        imp.tee_color.add_css_class(&course.color().to_string());

        let details = format!("Par {} - {}m", course.par(), course.meters());
        imp.course_details.set_label(&details);

        for (idx, hole) in course.holes().iter().enumerate() {
            let score_row = BdHoleEntry::new(hole, idx + 1);
            if card.is_some() {
                let hole_score = card.unwrap().get_hole_score(idx);
                if hole_score.strokes == 0 && hole_score.putts == 0 {
                    continue;
                }
                score_row.set_score(card.unwrap().get_hole_score(idx));
                score_row.set_details_visible(true);
                imp.completed_holes.append(&score_row);
                imp.hole_nr.set(imp.hole_nr.get() + 1);
                imp.par_so_far.set(imp.par_so_far.get() + hole.par());
            } else if idx == 0 {
                score_row.set_score_visible(true);
                imp.hole_box.append(&score_row);
            } else {
                imp.remaining_holes.append(&score_row);
            }
        }

        imp.update_score();
        if card.is_some() {
            imp.completed_holes.set_visible(true);
            self.emit_by_name::<()>("game-completed", &[]);
        } else {
            imp.hole_box.set_visible(true);
            imp.score_box.set_visible(true);
            imp.remaining_holes.set_visible(true);
        }
    }

    pub fn card(&self) -> Ref<'_, BdScoreCard> {
        self.imp().score_card.borrow()
    }

    pub fn set_course(&self, cid: &String) {
        self.set_data(Some(cid), None);
    }

    pub fn set_card(&self, card: &BdScoreCard) {
        self.set_data(None, Some(card));
    }

    #[template_callback]
    pub fn on_stroke_button_clicked(&self, _button: &gtk::Button) {
        let imp = self.imp();
        if let Some(row) = imp.hole_box.row_at_index(0) {
            if let Ok(hole) = row.downcast::<BdHoleEntry>() {
                hole.add_stroke();
            }
        }
        imp.skip_btn.set_visible(false);
        imp.done_btn.set_visible(true);
        imp.putt_btn.set_sensitive(true);
    }

    #[template_callback]
    pub fn on_putt_button_clicked(&self, _button: &gtk::Button) {
        if let Some(row) = self.imp().hole_box.row_at_index(0) {
            if let Ok(hole) = row.downcast::<BdHoleEntry>() {
                hole.add_putt();
            }
        }
    }

    #[template_callback]
    pub fn on_done_button_clicked(&self, _button: &gtk::Button) {
        let mut skipped = false;
        let imp = self.imp();
        imp.putt_btn.set_sensitive(false);
        imp.done_btn.set_visible(false);
        imp.skip_btn.set_visible(true);
        if let Some(row) = imp.hole_box.row_at_index(0) {
            if let Ok(hole) = row.downcast::<BdHoleEntry>() {
                let score = hole.get_score();
                if score.strokes == 0 && score.putts == 0 {
                    skipped = true;
                }
                let mut current = imp.score.get();
                let mut card = imp.score_card.borrow_mut();
                card.add_score(score);
                current.strokes += score.strokes;
                current.putts += score.putts;
                imp.score.set(current);
                if !skipped {
                    imp.hole_nr.set(imp.hole_nr.get() + 1);
                    imp.par_so_far.set(imp.par_so_far.get() + hole.get_par());
                    hole.set_details_visible(true);
                }
                imp.update_score();
            }
        }
        if let Some(row) = imp.hole_box.row_at_index(0) {
            imp.hole_box.remove(&row);
            if !skipped {
                imp.completed_holes.append(&row);
                imp.completed_holes.set_visible(true);
            }
        }
        if let Some(row) = imp.remaining_holes.row_at_index(0) {
            imp.remaining_holes.remove(&row);
            if imp.remaining_holes.first_child().is_none() {
                imp.remaining_holes.set_visible(false);
            }
            imp.hole_box.append(&row);
            if let Ok(hole) = row.downcast::<BdHoleEntry>() {
                hole.set_score_visible(true);
            }
        } else {
            imp.score_box.set_visible(false);
            imp.score_card.borrow_mut().save();
            self.emit_by_name::<()>("game-completed", &[]);
        }
    }
}
