use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;

use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};

use crate::config;
use crate::model::{course::BdCourse, golf::BdGolf, hole::BdHole, score_card::BdScoreCard};
use crate::utils::{DistanceUnit, HasId, TeeColor};

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy)]
pub struct HoleTee {
    pub color: TeeColor,
    pub distance: i32,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Hole {
    pub par: i32,
    pub handicap: i32,
    pub tees: Vec<HoleTee>,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct CourseTee {
    pub color: TeeColor,
    pub slope: i32,
    pub sss: f64,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Course {
    #[serde(default)]
    pub id: String,
    pub name: Option<String>,
    pub tees: Vec<CourseTee>,
    pub holes: Vec<Hole>,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Golf {
    #[serde(default)]
    pub id: String,
    pub name: String,
    pub unit: DistanceUnit,
    pub courses: Vec<Course>,
}

impl HasId for Golf {
    fn set_id(&mut self, id: &str) {
        self.id = String::from(id);
    }
}

fn load<T>(file: &PathBuf) -> Result<T, std::io::Error>
where
    T: DeserializeOwned,
{
    match std::fs::File::open(file) {
        Ok(f) => match serde_json::from_reader::<std::fs::File, T>(f) {
            Ok(g) => Ok(g),
            Err(e) => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, e)),
        },
        Err(e) => Err(e),
    }
}

fn load_dir<T>(path: PathBuf) -> Vec<T>
where
    T: DeserializeOwned + HasId,
{
    let mut courses: Vec<T> = Vec::new();

    if let Ok(entries) = fs::read_dir(&path) {
        for entry in entries.flatten() {
            let fname = entry.file_name().into_string().unwrap();

            let mut newpath = PathBuf::from(&path);
            newpath.push(&fname);

            if entry.file_type().unwrap().is_dir() {
                courses.extend(load_dir(newpath));
            } else {
                match load::<T>(&newpath) {
                    Ok(mut c) => {
                        let stem = newpath.file_stem().unwrap();
                        c.set_id(stem.to_str().unwrap());
                        courses.push(c)
                    }
                    Err(e) => eprintln!("Unable to parse {}: {}", fname, e),
                }
            }
        }
    }

    courses
}

#[derive(Debug, Default)]
pub struct BdDatabase {
    golfs: Vec<BdGolf>,
    courses: HashMap<String, BdCourse>,
    cards: Vec<BdScoreCard>,
}

impl BdDatabase {
    pub fn new() -> Self {
        let mut db = BdDatabase::default();

        let mut data_path = glib::user_data_dir();
        data_path.push("birdies");
        data_path.push("scores");
        db.cards = load_dir(data_path);

        db.cards.sort_by(|a, b| a.partial_cmp(b).unwrap());

        let mut data_path = glib::user_data_dir();
        data_path.push("birdies");
        data_path.push("courses");
        let mut golfs: Vec<Golf> = load_dir(data_path);

        let mut data_path = PathBuf::from(config::PKGDATADIR);
        data_path.push("courses");
        golfs.extend(load_dir(data_path));

        for golf in golfs {
            let mut my_golf = BdGolf::new();

            for (idx, course) in golf.courses.into_iter().enumerate() {
                let mut courses: HashMap<TeeColor, BdCourse> = Default::default();
                for tee in &course.tees {
                    let mut holes: Vec<BdHole> = vec![];
                    for hole in &course.holes {
                        for hole_tee in &hole.tees {
                            if hole_tee.color == tee.color {
                                holes.push(BdHole::new(
                                    hole.par,
                                    hole.handicap,
                                    hole_tee.distance,
                                    golf.unit,
                                ));
                                break;
                            }
                        }
                    }

                    let course_id = format!("{}.{}.{}", golf.id, idx, tee.color);
                    let course_name = if let Some(cname) = &course.name {
                        format!("{} - {}", golf.name, cname)
                    } else {
                        String::from(&golf.name)
                    };
                    let my_course = BdCourse::new(
                        String::from(&course_id),
                        course_name,
                        tee.slope,
                        tee.sss,
                        tee.color,
                        holes,
                    );

                    db.courses.insert(course_id, my_course.clone());
                    courses.insert(tee.color, my_course);
                }

                my_golf.add_course(courses);
            }

            db.golfs.push(my_golf);
        }

        db
    }

    pub fn cards(&self) -> &Vec<BdScoreCard> {
        &self.cards
    }

    pub fn course_by_id(&self, id: &String) -> Option<&BdCourse> {
        self.courses.get(id)
    }

    pub fn golfs(&self) -> &Vec<BdGolf> {
        &self.golfs
    }
}
