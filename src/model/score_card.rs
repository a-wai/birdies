use std::cmp::Ordering;
use std::fs::{create_dir_all, OpenOptions};

use glib::{self, gformat, GString};
use serde::{Deserialize, Serialize};

use crate::utils::{HasId, Score};

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct BdScoreCard {
    id: String,
    start: i64,
    duration: i64,
    scores: Vec<Score>,
}

impl HasId for BdScoreCard {
    fn set_id(&mut self, _id: &str) {}
}

impl PartialEq for BdScoreCard {
    fn eq(&self, other: &Self) -> bool {
        (self.id == other.id) && (self.start == other.start)
    }
}

impl PartialOrd for BdScoreCard {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.start.cmp(&other.start))
    }
}

impl BdScoreCard {
    pub fn new(id: &String) -> Self {
        BdScoreCard {
            id: String::from(id),
            start: glib::DateTime::now_local().unwrap().to_unix(),
            ..Default::default()
        }
    }

    pub fn from(card: &BdScoreCard) -> Self {
        BdScoreCard {
            id: String::from(&card.id),
            start: card.start,
            duration: card.duration,
            scores: card.scores.clone(),
        }
    }

    pub fn add_score(&mut self, score: Score) {
        self.scores.push(score);
    }

    pub fn date(&self) -> GString {
        let begin = glib::DateTime::from_unix_local(self.start).unwrap();
        begin.format("%Y/%m/%d %H:%M").unwrap()
    }

    pub fn duration(&self) -> GString {
        if self.duration > 3600 {
            let hours = self.duration / 3600;
            let minutes = (self.duration - hours * 3600) / 60;
            gformat!("{} hrs {:02} mins", hours, minutes)
        } else {
            gformat!("{} mins", self.duration / 60)
        }
    }

    pub fn get_hole_score(&self, idx: usize) -> Score {
        self.scores[idx]
    }

    pub fn get_total_score(&self) -> Score {
        let mut total = Score::default();
        for score in &self.scores {
            total.strokes += score.strokes;
            total.putts += score.putts;
        }

        total
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    pub fn save(&mut self) {
        let now = glib::DateTime::now_local().unwrap();
        let begin = glib::DateTime::from_unix_local(self.start).unwrap();
        self.duration = now.difference(&begin).as_seconds();
        let file = begin.format("%Y%m%d-%H%M%S").unwrap();
        let mut data_path = glib::user_data_dir();
        data_path.push("birdies");
        data_path.push("scores");
        data_path.push(begin.year().to_string());
        data_path.push(format!("{:02}", begin.month()));

        create_dir_all(&data_path).unwrap();

        data_path.push(file);
        data_path.set_extension("json");

        if let Ok(f) = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(data_path)
        {
            serde_json::to_writer(&f, self).unwrap();
        }
    }
}
