use crate::utils::DistanceUnit;

#[derive(Debug, Clone, Copy)]
enum Distance {
    Meters(i32),
    Yards(i32),
}

impl Distance {
    pub fn meters(&self) -> i32 {
        match self {
            Distance::Meters(m) => *m,
            Distance::Yards(y) => (y * 9144) / 10000,
        }
    }

    pub fn yards(&self) -> i32 {
        match self {
            Distance::Meters(m) => (m * 10000) / 9144,
            Distance::Yards(y) => *y,
        }
    }
}

impl Default for Distance {
    fn default() -> Self {
        Distance::Meters(0)
    }
}

#[derive(Debug, Default, Clone)]
pub struct BdHole {
    distance: Distance,
    handicap: i32,
    par: i32,
}

impl BdHole {
    pub fn new(par: i32, handicap: i32, dist: i32, unit: DistanceUnit) -> Self {
        let distance = match unit {
            DistanceUnit::Meters => Distance::Meters(dist),
            DistanceUnit::Yards => Distance::Yards(dist),
        };

        BdHole {
            distance,
            handicap,
            par,
        }
    }

    pub fn handicap(&self) -> i32 {
        self.handicap
    }

    pub fn meters(&self) -> i32 {
        self.distance.meters()
    }

    pub fn par(&self) -> i32 {
        self.par
    }

    pub fn yards(&self) -> i32 {
        self.distance.yards()
    }
}
