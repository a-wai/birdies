use crate::model::hole::BdHole;
use crate::utils::TeeColor;

#[derive(Debug, Default, Clone)]
pub struct BdCourse {
    id: String,
    name: String,
    color: TeeColor,
    slope: i32,
    sss: f64,
    holes: Vec<BdHole>,
}

impl BdCourse {
    pub fn new(
        id: String,
        name: String,
        slope: i32,
        sss: f64,
        color: TeeColor,
        holes: Vec<BdHole>,
    ) -> Self {
        Self {
            id,
            name,
            color,
            slope,
            sss,
            holes,
        }
    }

    pub fn color(&self) -> &TeeColor {
        &self.color
    }

    pub fn free_strokes(&self, index: f64) -> i32 {
        let fact = 18.0 / self.holes.len() as f64;
        let mut calc = (index * self.slope as f64) / 113.0;
        calc += self.sss - (fact * self.par() as f64);
        calc /= fact;

        calc.round() as i32
    }

    pub fn holes(&self) -> &Vec<BdHole> {
        &self.holes
    }

    pub fn holes_count(&self) -> usize {
        self.holes.len()
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    pub fn meters(&self) -> i32 {
        let mut dist = 0;

        for hole in &self.holes {
            dist += hole.meters();
        }

        dist
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn par(&self) -> i32 {
        let mut par = 0;

        for hole in &self.holes {
            par += hole.par();
        }

        par
    }

    pub fn par_through(&self, through: usize) -> i32 {
        let mut par = 0;

        for i in 0..through {
            par += self.holes[i].par();
        }

        par
    }

    pub fn yards(&self) -> i32 {
        let mut dist = 0;

        for hole in &self.holes {
            dist += hole.yards();
        }

        dist
    }
}
