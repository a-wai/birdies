use std::collections::HashMap;

use crate::model::course::BdCourse;
use crate::utils::TeeColor;

#[derive(Debug, Default)]
pub struct BdGolf {
    courses: Vec<HashMap<TeeColor, BdCourse>>,
}

impl BdGolf {
    pub fn new() -> Self {
        BdGolf::default()
    }

    pub fn add_course(&mut self, course: HashMap<TeeColor, BdCourse>) {
        self.courses.push(course);
    }

    pub fn courses(&self) -> &Vec<HashMap<TeeColor, BdCourse>> {
        &self.courses
    }
}
