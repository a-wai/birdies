use std::rc::Rc;

use adw::prelude::ActionRowExt;
use glib::{clone, subclass::prelude::*};
use gtk::{gio, glib, prelude::Cast, prelude::ObjectExt, prelude::WidgetExt};

use crate::app::BdApplication;
use crate::model::score_card::BdScoreCard;

mod imp {
    use adw::prelude::ExpanderRowExt;
    use adw::subclass::prelude::*;
    use glib::subclass::Signal;
    use gtk::{glib, prelude::Cast, CompositeTemplate};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;
    use std::rc::Rc;

    use crate::model::score_card::BdScoreCard;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/a-wai/Birdies/ui/cardsview.ui")]
    pub struct BdCardsView {
        #[template_child]
        pub cards_box: TemplateChild<gtk::ListBox>,

        // Internal state
        pub selected: RefCell<Rc<BdScoreCard>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BdCardsView {
        const NAME: &'static str = "BdCardsView";
        type Type = super::BdCardsView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BdCardsView {
        fn constructed(&self) {
            self.parent_constructed();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("card-selected").build()]);

            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for BdCardsView {}
    impl BoxImpl for BdCardsView {}

    impl BdCardsView {
        pub fn fold_all(&self) {
            let mut index: i32 = 0;
            while let Some(row) = self.cards_box.row_at_index(index) {
                if let Ok(expander) = row.downcast::<adw::ExpanderRow>() {
                    expander.set_expanded(false);
                }
                index += 1;
            }
        }
    }
}
glib::wrapper! {
    pub struct BdCardsView(ObjectSubclass<imp::BdCardsView>)
        @extends gtk::Widget, gtk::ListBox;
}

impl Default for BdCardsView {
    fn default() -> Self {
        Self::new()
    }
}

#[gtk::template_callbacks]
impl BdCardsView {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }

    pub fn add_card(&self, card: &BdScoreCard) {
        let app = gio::Application::default()
            .expect("Failed to retrieve application singleton")
            .downcast::<BdApplication>()
            .unwrap();

        let db = app.get_db();

        match db.course_by_id(card.id()) {
            Some(course) => {
                let row = adw::ActionRow::builder()
                    .title(course.name())
                    .subtitle(format!("{} - {}", card.date(), card.duration()))
                    .activatable(true)
                    .build();

                let mut par_so_far = 0;
                let mut strokes = 0;

                for (idx, hole) in course.holes().iter().enumerate() {
                    let score = card.get_hole_score(idx);
                    if score.strokes > 0 {
                        par_so_far += hole.par();
                        strokes += score.strokes + score.putts;
                    }
                }

                let strokes_label = gtk::Label::builder().label(format!("{}", strokes)).build();

                let score = strokes - par_so_far;
                let score_label = gtk::Label::builder()
                    .label(format!("({:+})", score))
                    .build();

                if score < -1 {
                    score_label.set_css_classes(&["success"]);
                } else if score == -1 {
                    score_label.set_css_classes(&["accent"]);
                } else if score == 1 {
                    score_label.set_css_classes(&["warning"]);
                } else if score > 1 {
                    score_label.set_css_classes(&["error"]);
                }

                row.add_suffix(&strokes_label);
                row.add_suffix(&score_label);

                let card_rc = Rc::new(BdScoreCard::from(card));
                row.connect_activated(clone!(
                    #[weak(rename_to = view)]
                    self,
                    #[strong]
                    card_rc,
                    move |_| {
                        view.imp().selected.replace(card_rc.clone());
                        view.emit_by_name::<()>("card-selected", &[]);
                    }
                ));

                self.imp().cards_box.prepend(&row);
            }
            _ => eprintln!("Unable to find course with id {}", card.id()),
        }
    }

    pub fn selected_card(&self) -> Rc<BdScoreCard> {
        self.imp().selected.borrow().clone()
    }
}
