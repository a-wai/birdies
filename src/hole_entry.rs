use adw::prelude::{ActionRowExt, PreferencesRowExt};
use glib::subclass::prelude::*;
use gtk::{glib, prelude::PopoverExt, prelude::WidgetExt};

use crate::model::hole::BdHole;
use crate::utils::Score;

mod imp {
    use adw::subclass::prelude::*;
    use gtk::{glib, prelude::WidgetExt, CompositeTemplate};
    use once_cell::sync::OnceCell;
    use std::cell::Cell;

    use crate::model::hole::BdHole;
    use crate::utils::Score;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/a-wai/Birdies/ui/hole.ui")]
    pub struct BdHoleEntry {
        #[template_child]
        pub gir_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub strokes_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub score_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub details_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub details_popup: TemplateChild<gtk::Popover>,
        #[template_child]
        pub putts_label: TemplateChild<gtk::Label>,

        pub hole: OnceCell<BdHole>,
        pub score: Cell<Score>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BdHoleEntry {
        const NAME: &'static str = "BdHoleEntry";
        type Type = super::BdHoleEntry;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BdHoleEntry {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }

    impl WidgetImpl for BdHoleEntry {}
    impl ListBoxRowImpl for BdHoleEntry {}
    impl PreferencesRowImpl for BdHoleEntry {}
    impl ActionRowImpl for BdHoleEntry {}

    impl BdHoleEntry {
        pub fn update_score(&self) {
            let score = self.score.get();
            let strokes = score.strokes + score.putts;
            let par = if let Some(hole) = self.hole.get() {
                hole.par()
            } else {
                eprintln!("Called update_score() on non-existent hole!");
                strokes
            };

            if score.putts > 0 {
                self.putts_label.set_label(&format!("{}", score.putts));
            }

            let score = strokes - par;
            self.strokes_label.set_label(&format!("{}", strokes));
            self.score_label.set_label(&format!("({:+})", score));

            if score < -1 {
                self.score_label.set_css_classes(&["success"]);
            } else if score == -1 {
                self.score_label.set_css_classes(&["accent"]);
            } else if score == 1 {
                self.score_label.set_css_classes(&["warning"]);
            } else if score > 1 {
                self.score_label.set_css_classes(&["error"]);
            } else {
                self.score_label.set_css_classes(&[]);
            }
        }
    }
}

glib::wrapper! {
    pub struct BdHoleEntry(ObjectSubclass<imp::BdHoleEntry>)
        @extends gtk::Widget, gtk::ListBoxRow, adw::PreferencesRow, adw::ActionRow;
}

#[gtk::template_callbacks]
impl BdHoleEntry {
    pub fn new(hole: &BdHole, number: usize) -> Self {
        let entry: BdHoleEntry = glib::Object::builder().build();
        let imp = entry.imp();

        let title = format!("Hole {}", number);
        let subtitle = format!("Par {} - {}m", hole.par(), hole.meters());

        imp.obj().set_title(&title);
        imp.obj().set_subtitle(&subtitle);

        imp.hole
            .set(hole.clone())
            .unwrap_or_else(|_| panic!("{} already set!", title));
        imp.score.set(Score::default());
        imp.update_score();

        entry.set_score_visible(false);
        entry
    }

    pub fn add_stroke(&self) {
        let imp = self.imp();
        let mut score = imp.score.get();
        score.add_stroke();
        imp.score.set(score);
        imp.update_score();
    }

    pub fn add_putt(&self) {
        let imp = self.imp();
        let mut score = imp.score.get();
        let par = if let Some(hole) = imp.hole.get() {
            hole.par()
        } else {
            eprintln!("Called add_putt() on non-existent hole!");
            score.strokes
        };
        if !imp.gir_label.get_visible() && score.strokes <= (par - 2) {
            imp.gir_label.set_visible(true);
            score.set_gir(true);
        }
        score.add_putt();
        imp.score.set(score);
        imp.update_score();
    }

    pub fn get_par(&self) -> i32 {
        match self.imp().hole.get() {
            Some(h) => h.par(),
            _ => 0,
        }
    }

    pub fn get_score(&self) -> Score {
        self.imp().score.get()
    }

    pub fn set_score(&self, score: Score) {
        let imp = self.imp();
        if score.gir {
            imp.gir_label.set_visible(true);
        }
        imp.score.set(score);
        imp.update_score();
        self.set_score_visible(true);
    }

    pub fn set_details_visible(&self, visible: bool) {
        self.imp().details_btn.set_visible(visible);
    }

    pub fn set_score_visible(&self, visible: bool) {
        let imp = self.imp();
        imp.strokes_label.set_visible(visible);
        imp.score_label.set_visible(visible);
    }

    #[template_callback]
    pub fn on_details_button_clicked(&self, _button: &gtk::Button) {
        let imp = self.imp();
        imp.details_popup.popup();
    }
}
