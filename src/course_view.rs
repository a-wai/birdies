use std::collections::HashMap;
use std::rc::Rc;

use adw::prelude::{ActionRowExt, ExpanderRowExt, PreferencesRowExt};
use glib::subclass::prelude::*;
use gtk::{
    glib::{self, clone},
    prelude::ButtonExt,
    prelude::ObjectExt,
    prelude::WidgetExt,
};

use crate::model::course::BdCourse;
use crate::utils::TeeColor;

mod imp {
    use adw::prelude::ExpanderRowExt;
    use adw::subclass::prelude::*;
    use glib::subclass::Signal;
    use gtk::{glib, prelude::Cast, CompositeTemplate};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;
    use std::rc::Rc;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/a-wai/Birdies/ui/courseview.ui")]
    pub struct BdCourseView {
        #[template_child]
        pub course_box: TemplateChild<gtk::ListBox>,

        // Internal state
        pub selected: RefCell<Rc<String>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BdCourseView {
        const NAME: &'static str = "BdCourseView";
        type Type = super::BdCourseView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BdCourseView {
        fn constructed(&self) {
            self.parent_constructed();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("course-selected").build()]);

            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for BdCourseView {}
    impl BoxImpl for BdCourseView {}

    impl BdCourseView {
        pub fn fold_all(&self) {
            let mut index: i32 = 0;
            while let Some(row) = self.course_box.row_at_index(index) {
                if let Ok(expander) = row.downcast::<adw::ExpanderRow>() {
                    expander.set_expanded(false);
                }
                index += 1;
            }
        }
    }
}
glib::wrapper! {
    pub struct BdCourseView(ObjectSubclass<imp::BdCourseView>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for BdCourseView {
    fn default() -> Self {
        Self::new()
    }
}

#[gtk::template_callbacks]
impl BdCourseView {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }

    pub fn add_course(&self, course: &HashMap<TeeColor, BdCourse>) {
        let row = adw::ExpanderRow::builder().build();

        if let Some(course) = course.values().next() {
            row.set_title(course.name());
            row.set_subtitle(&format!("{} holes", course.holes_count()));
        }

        for (color, course) in course {
            let subrow = adw::ActionRow::builder()
                .title(format!("{}m", course.meters()))
                .build();

            let pre_btn = gtk::Button::builder().valign(gtk::Align::Center).build();
            pre_btn.add_css_class("circular");
            pre_btn.add_css_class("opaque");
            pre_btn.add_css_class(&color.to_string());

            subrow.add_prefix(&pre_btn);

            let post_btn = gtk::Button::builder()
                .label("Play")
                .icon_name("media-playback-start-symbolic")
                .valign(gtk::Align::Center)
                .build();

            let course_rc = Rc::new(String::from(course.id()));
            post_btn.connect_clicked(clone!(
                #[weak(rename_to = view)]
                self,
                #[strong]
                course_rc,
                move |_| {
                    view.imp().fold_all();
                    view.imp().selected.replace(course_rc.clone());
                    view.emit_by_name::<()>("course-selected", &[]);
                }
            ));

            subrow.add_suffix(&post_btn);
            row.add_row(&subrow);
        }

        self.imp().course_box.append(&row);
    }

    pub fn selected_course(&self) -> Rc<String> {
        self.imp().selected.borrow().clone()
    }
}
